package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void echo_return5(){
        assertEquals("Testing if echo returns the given number", 5, App.echo(5));
    }

    @Test
    public void oneMore_return6(){
        assertEquals("Testing if oneMore returns given number + 1", 6, App.oneMore(5));
    }
}
